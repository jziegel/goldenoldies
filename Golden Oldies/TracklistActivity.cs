using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Golden_Oldies.Resources.layout;

namespace Golden_Oldies
{
    [Activity(Label = "Tracklist")]
    public class TracklistActivity : Activity, AdapterView.IOnItemClickListener
    {
        private ListView _listViewTracklist;
        private Button _buttonMenu;
        private MusicArrayAdapter _musicArrayAdapter;

        // Debugging
        private string TAG = "TracklistActivity - ";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnCreate called");
            base.OnCreate(savedInstanceState);
            this.RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.Tracklist);

            // Create assets
            MusicSQLOpenHelper musicSQLOpenHelper;

            // Create custom typeface to views
            Typeface customTypeface = Typeface.CreateFromAsset(this.Assets, Constants.COMFORTAA_REGULAR_PATH);

            // Assign views
            _buttonMenu = FindViewById<Button>(Resource.Id.buttonMenu);

            // Set custom typeface
            _buttonMenu.SetTypeface(customTypeface, TypefaceStyle.Normal);

            // Associate the list view
            _listViewTracklist = FindViewById<ListView>(Resource.Id.listViewTracklist);

            // Open a connection to the music database
            musicSQLOpenHelper = new MusicSQLOpenHelper(this);

            // Get the list of track names and artists
            int[] IDs = musicSQLOpenHelper.GetIDs();

            // If the list is not empty, display the contents of the list
            if (IDs != null)
            {
                _musicArrayAdapter = new MusicArrayAdapter(this, IDs);
                System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Adding tracks to ListView");
                _listViewTracklist.Adapter = _musicArrayAdapter;
            }

            _listViewTracklist.FastScrollEnabled = true;

            _listViewTracklist.OnItemClickListener = this;

            // Handle menu button click event
            _buttonMenu.Click += delegate
            {
                this.Finish();
            };
        }

        public void OnItemClick(AdapterView parent, View view, int position, long id)
        {
            //TextView textViewTrackID = view.FindViewById<TextView>(Resource.Id.textViewTrackID);
            string trackTitle = view.FindViewById<TextView>(Resource.Id.textViewTrackTitle).Text;
            string trackArtistAlbum = view.FindViewById<TextView>(Resource.Id.textViewTrackArtistAlbum).Text;

            // Get the path of the track that was selected
            int trackId = _musicArrayAdapter[position];

            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Track ID " +
                Java.Lang.String.ValueOf(trackId) + " " + trackTitle + " " + trackArtistAlbum + " selected");

            // Place the track id in the intent
            Intent intentPlayActivity = new Intent(this, typeof(PlayActivity));
            intentPlayActivity.PutExtra("ID", trackId);

            // Start the play activity
            StartActivity(intentPlayActivity);

            // Finish this activity
            Finish();
        }
    }
}