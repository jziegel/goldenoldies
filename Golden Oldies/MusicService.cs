using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using Golden_Oldies.Resources.layout;

namespace Golden_Oldies
{
    [Service]
    [IntentFilter(new String[] { "com.Golden_Oldies.MusicService" })]
    public class MusicService : Service, MediaPlayer.IOnErrorListener, MediaPlayer.IOnCompletionListener
    {
        private MusicServiceBinder _musicServiceBinder;
        private MediaPlayer _mediaPlayer;
        public MediaPlayer MediaPlayer
        {
            get
            {
                return _mediaPlayer;
            }

            set
            {
                _mediaPlayer = value;
            }
        }
        private int length;

        // Debugging
        private string TAG = "MusicService - ";

        public MusicService()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "constructor called");
        }

        public override void OnCreate()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnCreate called");
            base.OnCreate();
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnStartCommand called");
            return StartCommandResult.Sticky;
        }

        public override void OnDestroy()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnDestroy called");
            base.OnDestroy();
            if (_mediaPlayer != null)
            {
                try
                {
                    _mediaPlayer.Stop();
                    _mediaPlayer.Release();
                }
                finally
                {
                    _mediaPlayer = null;
                }
            }
        }

        public override IBinder OnBind(Intent intent)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnBind called");
            _musicServiceBinder = new MusicServiceBinder(this);
            return _musicServiceBinder;
        }

        public bool OnError(MediaPlayer mp, [GeneratedEnum] MediaError what, int extra)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnError called");
            Toast.MakeText(this, "Failed to play music", ToastLength.Short).Show();
            if (_mediaPlayer != null)
            {
                try
                {
                    _mediaPlayer.Stop();
                    _mediaPlayer.Release();
                }
                finally
                {
                    _mediaPlayer = null;
                }
            }
            return false;
        }

        // Interface which will be used to connect to the active PlayActivity
        public interface Callbacks
        {
            void PlaybackComplete();
        }

        private Callbacks _callbacks;

        public void setCallbacks(Callbacks callbacks)
        {
            _callbacks = callbacks;
        }

        public void SetEventListeners()
        {
            _mediaPlayer.SetOnErrorListener(this);
            _mediaPlayer.SetOnCompletionListener(this);
        }

        public void SetTrackAndPlay(string trackPath)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "SetTrackAndPlay called");

            if (_mediaPlayer != null)
            {
                _mediaPlayer.Reset();
                _mediaPlayer.Release();
                _mediaPlayer = null;
            }
            _mediaPlayer = MediaPlayer.Create(this, Android.Net.Uri.Parse("file://" + trackPath));
            _mediaPlayer.Start();
            _mediaPlayer.SetOnCompletionListener(this);
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Started playing file " + trackPath);
        }

        public void PausePlayback()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "PausePlayback called");
            if (_mediaPlayer != null && _mediaPlayer.IsPlaying)
            {
                _mediaPlayer.Pause();
                length = _mediaPlayer.CurrentPosition;
            }
        }

        public void ResumePlayback()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "ResumePlayback called");
            if (_mediaPlayer != null && !_mediaPlayer.IsPlaying)
            {
                _mediaPlayer.SeekTo(length);
                _mediaPlayer.Start();
            }
        }

        public void StopPlayback()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "StopPlayback called");
            if (_mediaPlayer != null)
                _mediaPlayer.Stop();
        }

        public void SeekToPosition(int position)
        {
            if (_mediaPlayer != null)
                _mediaPlayer.SeekTo(position);
        }

        public bool IsPlaying()
        {
            if (_mediaPlayer != null)
                return _mediaPlayer.IsPlaying;
            else
                return false;
        }

        public int GetDuration()
        {
            int duration = 0;
            if (_mediaPlayer != null)
                duration = _mediaPlayer.Duration;
            return duration;
        }

        public int GetCurrentPosition()
        {
            int currentPosition = 0;
            if (_mediaPlayer != null)
                currentPosition = _mediaPlayer.CurrentPosition;
            return currentPosition;
        }

        public void OnCompletion(MediaPlayer mp)
        {
            _callbacks.PlaybackComplete();
        }

        public bool IsNull()
        {
            if (_mediaPlayer == null)
                return true;
            else
                return false;
        }
    }
}