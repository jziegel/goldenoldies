using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.OS;

namespace Golden_Oldies
{
    public class MusicServiceBinder : Binder
    {
        private MusicService _musicService;
        private string TAG = "MusicServiceBinder - ";

        public MusicServiceBinder (MusicService musicService)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "constructor called");
            this._musicService = musicService;
        }

        public MusicService GetMusicService()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "GetMusicService called");
            return _musicService;
        }
    }
}