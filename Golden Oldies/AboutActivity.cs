using System;

using Android.App;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using Android.Media;

namespace Golden_Oldies
{
    [Activity(Label = "About")]
    public class AboutActivity : Activity, MediaPlayer.IOnCompletionListener
    {
        // UI
        private Button _buttonMenu;
        private TextView _textViewAboutIntro;
        private VideoView _videoView;

        // Debugging
        private string TAG = "AboutActivity - ";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnCreate called");
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.About);

            // Create custom typeface
            Typeface customTypeface = Typeface.CreateFromAsset(this.Assets, Constants.COMFORTAA_REGULAR_PATH);

            // Assign views           
            _buttonMenu = FindViewById<Button>(Resource.Id.buttonMenuAbout);
            _textViewAboutIntro = FindViewById<TextView>(Resource.Id.textViewAboutIntro);
            _videoView = FindViewById<VideoView>(Resource.Id.videoView);

            // Set custom typeface         
            _buttonMenu.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _textViewAboutIntro.SetTypeface(customTypeface, TypefaceStyle.Normal);

            // Set the video to the videoview
            string path = "android.resource://" + PackageName + "/" + Resource.Raw.about;
            _videoView.SetVideoURI(Android.Net.Uri.Parse(path));
            _videoView.SetMediaController(new MediaController(this));

            // Start playing the video
            _videoView.Start();

            // Set oncompletionlistener
            _videoView.SetOnCompletionListener(this);

            _buttonMenu.Click += _buttonMenu_Click;
        }

        private void _buttonMenu_Click(object sender, EventArgs e)
        {
            _videoView.StopPlayback();
            this.Finish();
        }

        // Reset the videoview when done playing
        public void OnCompletion(MediaPlayer mp)
        {
            mp.Reset();
        }
    }
}