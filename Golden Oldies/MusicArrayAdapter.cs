using System;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Media;

namespace Golden_Oldies
{
    class MusicArrayAdapter : BaseAdapter<int>
    {
        private Context _context;
        private int[] _trackIDs;
        private TextView _textViewTrackName;
        private TextView _textViewTrackArtistAlbum;
        private ImageView _imageViewAlbumArt;
        private MusicSQLOpenHelper _musicSQLOpenHelper;

        public override int Count
        {
            get
            {
                return _trackIDs.Length;
            }
        }

        // Get the track id of the clicked row
        public override int this[int position]
        {
            get
            {
                return _trackIDs[position];
            }
        }

        public MusicArrayAdapter(Context context, int[] trackIDs) : base()
        {
            this._context = context;
            this._trackIDs = trackIDs;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            // Create a layout inflater to inflate the row view
            LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

            //if (rowView == null)
            View rowView = inflater.Inflate(Resource.Layout.TracklistRow, null);

            // Create a custom typeface
            Typeface customTypeface = Typeface.CreateFromAsset(Application.Context.Assets, Constants.COMFORTAA_REGULAR_PATH);

            _musicSQLOpenHelper = new MusicSQLOpenHelper(Application.Context);

            // Associate view assets
            _textViewTrackName = rowView.FindViewById<TextView>(Resource.Id.textViewTrackTitle);
            _textViewTrackArtistAlbum = rowView.FindViewById<TextView>(Resource.Id.textViewTrackArtistAlbum);
            _imageViewAlbumArt = rowView.FindViewById<ImageView>(Resource.Id.imageViewAlbumArt);

            // Set the custom typeface for the text views
            _textViewTrackName.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _textViewTrackArtistAlbum.SetTypeface(customTypeface, TypefaceStyle.Normal);

            // Get the track name, album and artist from the database
            string trackName = _musicSQLOpenHelper.GetTrackName(_trackIDs[position]);
            string trackArtistAlbum = _musicSQLOpenHelper.GetTrackArtist(_trackIDs[position]) + " - " +
                _musicSQLOpenHelper.GetTrackAlbum(_trackIDs[position]);

            // Set the text for the text views
            _textViewTrackName.SetText(trackName, TextView.BufferType.Normal);
            _textViewTrackArtistAlbum.SetText(trackArtistAlbum, TextView.BufferType.Normal);

            // Try to get the album art or else show a default image
            try
            {
                // Create a MediaMetadataRetriever to get the album art
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

                // Set the path for the MediaMetadataRetriever
                mediaMetadataRetriever.SetDataSource(_musicSQLOpenHelper.GetTrackPath(_trackIDs[position]));

                // Create a bitmap image resource and set it to the imageview
                byte[] bytes = mediaMetadataRetriever.GetEmbeddedPicture();
                Bitmap bitmap = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                _imageViewAlbumArt.SetImageBitmap(bitmap);
            }
            catch (Exception)
            {
                // No album art found, show a default image
                _imageViewAlbumArt.SetImageResource(Resource.Drawable.default_album);
            }

            // Return the created rowView
            return rowView;
        }

        // Return the track id of the clicked row
        public override long GetItemId(int position)
        {
            return this[position];
        }
    }

}