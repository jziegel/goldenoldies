﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Content;
using Android.Media;

using Java.IO;

namespace Golden_Oldies
{
    [Activity(Label = "Golden Oldies", MainLauncher = true, Icon = "@drawable/ic_launcher")]
    public class MainActivity : Activity
    {
        // For runnables
        private static Context _applicationContext;

        // UI
        private Button _buttonListen;
        private Button _buttonSearch;
        private Button _buttonAbout;
        private TextView _textViewTitle;

        // Track DB update status
        private static bool _DBUpdated = false;

        // Debugging
        private static string TAG = "MenuActivity - ";

        protected override void OnCreate(Bundle bundle)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnCreate called");
            base.OnCreate(bundle);
            this.RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.Main);

            // Create custom typeface to views
            Typeface customTypeface = Typeface.CreateFromAsset(this.Assets, Constants.COMFORTAA_REGULAR_PATH);

            // Assign views
            _buttonListen = FindViewById<Button>(Resource.Id.buttonListen);
            _buttonSearch = FindViewById<Button>(Resource.Id.buttonSearch);
            _buttonAbout = FindViewById<Button>(Resource.Id.buttonAbout);
            _textViewTitle = FindViewById<TextView>(Resource.Id.textViewTitle);

            // Set custom typeface
            _buttonListen.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _buttonSearch.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _buttonAbout.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _textViewTitle.SetTypeface(customTypeface, TypefaceStyle.Normal);

            // Play button click delegate
            _buttonListen.Click += delegate
            {
                var intentTracklist = new Intent(this, typeof(TracklistActivity));
                StartActivity(intentTracklist);
            };

            // Search button click delegate
            _buttonSearch.Click += delegate
            {
                var uri = Android.Net.Uri.Parse(Constants.SEARCH_URL);
                var intentSearch = new Intent(Intent.ActionView, uri);
                StartActivity(intentSearch);
            };

            // About button click delegate
            _buttonAbout.Click += delegate
            {
                var intentAbout = new Intent(this, typeof(AboutActivity));
                StartActivity(intentAbout);
            };

            _applicationContext = ApplicationContext;

            // Scan the sdcard's music folder and add music files to database
            // Only if the DB is not updated already so that update is not triggered after an orientation change
            if (!_DBUpdated)
                UpdateDatabase.Run();
        }

        // A runnable to scan the music folder and add file infor to the database
        private static Java.Lang.Runnable UpdateDatabase = new Java.Lang.Runnable(() =>
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "UpdateDatabase called");

            // Scan folder and add music files to database
            List<string> filePaths = new List<string>();
            File file = Android.OS.Environment.GetExternalStoragePublicDirectory(Constants.MUSIC_DIRECTORY);

            File[] musicFiles = file.ListFiles();

            if (musicFiles != null)
            {
                for (int i = 0; i < musicFiles.Length; i++)
                {
                    filePaths.Add(musicFiles[i].AbsolutePath);
                    System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Found track " + musicFiles[i].ToString());
                }
            }

            // Open a connection to the database
            MusicSQLOpenHelper musicSQLOpenHelper = new MusicSQLOpenHelper(_applicationContext);
            musicSQLOpenHelper.ClearData();

            // Create metadata retriever to retrieve music meta data
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            string trackTitle;
            string trackArtist;
            string trackAlbum;

            // Add tracks to the database
            foreach (String path in filePaths)
            {
                // Set the path for the MediaMetadataRetriever
                mediaMetadataRetriever.SetDataSource(path);

                // Try to get the track title or else use a default value
                try
                {
                    trackTitle = mediaMetadataRetriever.ExtractMetadata(MetadataKey.Title);
                }
                catch (Exception)
                {
                    trackTitle = _applicationContext.GetString(Resource.String.DefaultTrackTitle);
                }

                // Try to get the track artist or else use a default value
                try
                {
                    trackArtist = mediaMetadataRetriever.ExtractMetadata(MetadataKey.Artist);
                }
                catch (Exception)
                {
                    trackArtist = _applicationContext.GetString(Resource.String.DefaultArtist);
                }

                // Try to get the track album or else use a default value
                try
                {
                    trackAlbum = mediaMetadataRetriever.ExtractMetadata(MetadataKey.Album);
                }
                catch (Exception)
                {
                    trackAlbum = _applicationContext.GetString(Resource.String.DefaultAlbum);
                }

                // Add the track to the database
                musicSQLOpenHelper.InsertData(trackTitle, trackArtist, trackAlbum, path);
                System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Added track info " + trackTitle
                    + " by " + trackArtist + " (" + trackAlbum + ")");
            }

            // Close the connection to the database
            musicSQLOpenHelper.Close();

            _DBUpdated = true;
        });
    }
}

