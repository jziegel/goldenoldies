using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Golden_Oldies
{
    class Constants
    {
        // Paths

        // This is the folder where the music should be placed, but it can be any folder as long as the path is set here
        public static string MUSIC_DIRECTORY = "Music";
        public static string SEARCH_URL = "http://www.google.com";
        public static string COMFORTAA_REGULAR_PATH = "fonts/ComfortaaRegular.ttf";

        // Debugging
        public static string ALERT = "APP ALERT: ";
        public static string ERROR = "APP ERROR: ";
    }
}