using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using Android.Media;
using Java.Lang;
using Android.Views;
using Android.Views.Animations;
using Android.Runtime;

namespace Golden_Oldies.Resources.layout
{
    [Activity(Label = "Play")]

    public class PlayActivity : Activity, MusicService.Callbacks
    {
        // For maintaining services
        private bool _isBound = false;
        public bool IsBound
        {
            get
            {
                return _isBound;
            }

            set
            {
                _isBound = value;
            }
        }

        private bool _isConfigurationChange = false;
        public bool IsConfigurationChange
        {
            get
            {
                return _isConfigurationChange;
            }

            set
            {
                _isConfigurationChange = value;
            }
        }

        private MusicServiceBinder _musicServiceBinder;
        public MusicServiceBinder MusicServiceBinder
        {
            get
            {
                return _musicServiceBinder;
            }

            set
            {
                _musicServiceBinder = value;
            }
        }

        private MusicServiceConnection _musicServiceConnection;
        private static MusicService _musicService;

        // UI
        private ImageButton _buttonPlayPause;
        private ImageButton _buttonPrevious;
        private ImageButton _buttonNext;
        private ToggleButton _toggleButtonRepeat;
        private Button _buttonTracklist;
        private TextView _textViewTrackTitle;
        private TextView _textViewTrackArtistAlbum;
        private ImageView _imageViewAlbumArt;
        private static SeekBar _seekBarTrack;

        // Database
        private MusicSQLOpenHelper _musicSQLOpenHelper;

        // Music control
        private int[] _playlist;
        private int _playlistPosition;
        private MediaMetadataRetriever _mediaMetadataRetriever;
        private bool _repeat;
        private static int SEEKBAR_UPDATE_DELAY = 1000;

        // Debugging
        private string TAG = "PlayActivity - ";

        // Animation
        private static int ANIMATION_DELAY = 200;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "onCreate called");
            base.OnCreate(savedInstanceState);
            this.RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.Play);

            // Create custom typeface to views
            Typeface customTypeface = Typeface.CreateFromAsset(this.Assets, Constants.COMFORTAA_REGULAR_PATH);

            // Assign views
            _buttonPlayPause = FindViewById<ImageButton>(Resource.Id.buttonPlayPause);
            _buttonPrevious = FindViewById<ImageButton>(Resource.Id.buttonPrevious);
            _buttonNext = FindViewById<ImageButton>(Resource.Id.buttonNext);
            _buttonTracklist = FindViewById<Button>(Resource.Id.buttonTracklist);
            _toggleButtonRepeat = FindViewById<ToggleButton>(Resource.Id.toggleButtonRepeat);
            _textViewTrackTitle = FindViewById<TextView>(Resource.Id.textViewTrackTitle);
            _textViewTrackArtistAlbum = FindViewById<TextView>(Resource.Id.textViewTrackArtistAlbum);
            _imageViewAlbumArt = FindViewById<ImageView>(Resource.Id.imageViewAlbumArt);
            _seekBarTrack = FindViewById<SeekBar>(Resource.Id.seekBarTrack);

            // Set custom typeface
            _textViewTrackTitle.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _textViewTrackArtistAlbum.SetTypeface(customTypeface, TypefaceStyle.Normal);
            _buttonTracklist.SetTypeface(customTypeface, TypefaceStyle.Normal);

            // Set repeat to false
            _repeat = false;

            // Get the intent and get the first track ID from it
            int firstTrackId = Intent.GetIntExtra("ID", -1);
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "ID " + firstTrackId.ToString() + " retrieved from Intent.");

            // Open access to the database
            _musicSQLOpenHelper = new MusicSQLOpenHelper(this);

            // Initialise a MediaMetadataRetriever to retrieve meta information from the track
            _mediaMetadataRetriever = new MediaMetadataRetriever();

            // Create a playlist from the tracks in the database
            _playlist = _musicSQLOpenHelper.GetIDs();

            // Set the current track as the starting point in the playlist
            for (int i = 0; i < _playlist.Length; i++)
            {
                if (_playlist[i] == firstTrackId)
                {
                    _playlistPosition = i;
                    System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "Playlist position "
                        + Java.Lang.String.ValueOf(_playlistPosition));
                }
            }

            // Update the UI
            UpdateUI();

            // Set UI event listeners
            _seekBarTrack.ProgressChanged += _seekBarTrack_ProgressChanged;
            _buttonPlayPause.Click += _buttonPlayPause_Click;
            _buttonPrevious.Click += _buttonPrevious_Click;
            _buttonNext.Click += _buttonNext_Click;
            _buttonTracklist.Click += _buttonTracklist_Click;
            _toggleButtonRepeat.Click += _toggleButtonRepeat_Click;

            // Get the previous musicserviceconnection if there was a configuration change
            _musicServiceConnection = LastNonConfigurationInstance as MusicServiceConnection;
            if (_musicServiceConnection != null)
                _musicServiceBinder = _musicServiceConnection.musicServiceBinder;

            StartService(new Intent(this, typeof(MusicService)));
        }

        protected override void OnStart()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnStart called");
            base.OnStart();

            // Start the Music Service
            if (_musicServiceConnection == null)
                _musicServiceConnection = new MusicServiceConnection(this);

            // Bind the service 
            DoBindService();
        }

        // Release resources when the activity is destroyed
        protected override void OnDestroy()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnDestroy called");
            base.OnDestroy();

            if (!_isConfigurationChange)
            {
                StopService(new Intent(this, typeof(MusicService)));
                DoUnbindService();
            }
            _musicSQLOpenHelper.Close();
        }


        public void DoBindService()
        {
            ApplicationContext.BindService(new Intent(this, typeof(MusicService)), _musicServiceConnection, Bind.AutoCreate);
            _isBound = true;
        }

        public void DoUnbindService()
        {
            if (_isBound)
            {
                ApplicationContext.UnbindService(_musicServiceConnection);
                _isBound = false;
            }
        }

        /// <summary>
        /// Return the service connection if there is a configuration change
        /// </summary>
        /// <returns>A music service connection</returns>
        public override Java.Lang.Object OnRetainNonConfigurationInstance()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnRetainNonConfigurationInstance called");
            base.OnRetainNonConfigurationInstance();
            _isConfigurationChange = true;
            return _musicServiceConnection;
        }

        /// <summary>
        /// This method is called by MusicServiceConnection once a connection has been established, from
        /// its OnServiceConnected callback.
        /// </summary>
        public void StartPlayback()
        {
            // If the music service isn't null, this means that there is already a service running
            // so it is a configuration change.
            if (_musicService == null)
            {
                _musicService = _musicServiceBinder.GetMusicService();
                PlayTrack();
                _musicService.SetEventListeners();
            }
            // If musi
            else
            {
                if (!_isConfigurationChange)
                {
                    _musicService = _musicServiceBinder.GetMusicService();
                    PlayTrack();
                }
            }
        }

        /// <summary>
        /// Handle the playpause button click event
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">EventArgs</param>
        private void _buttonPlayPause_Click(object sender, EventArgs e)
        {
            if (_isBound)
            {
                // Pause music if it is playing
                if (_musicService.IsPlaying())
                {
                    _musicService.PausePlayback();
                    _buttonPlayPause.SetImageResource(Resource.Drawable.button_play);
                }
                // Resume music if it is paused
                else
                {
                    _musicService.ResumePlayback();
                    _buttonPlayPause.SetImageResource(Resource.Drawable.button_pause);
                    _seekBarTrack.PostDelayed(UpdateSeekBarPosition, SEEKBAR_UPDATE_DELAY);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "_isBound false");
            }
        }

        /// <summary>
        /// Handle the previous button click event
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">EventArgs</param>
        private void _buttonPrevious_Click(object sender, EventArgs e)
        {
            if (_isBound)
            {
                // If the playlist position is larger than 0, decrement the position and start playing
                if (_playlist.Length != 0 && _playlistPosition > 0)
                {
                    _playlistPosition -= 1;
                    PlayTrack();
                }

                // If the position is equal to zero, set the position to the end of the playlist
                else if (_playlist.Length != 0 && _playlistPosition == 0)
                {
                    _playlistPosition = _playlist.Length - 1;
                    PlayTrack();
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "_isBound false");
            }
        }

        /// <summary>
        /// Handle the next button click event
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">EventArgs</param>
        private void _buttonNext_Click(object sender, EventArgs e)
        {
            if (_isBound)
            {
                // If the playlist position is not the last on the playlist, increment the position and start playing
                if (_playlist.Length != 0 && _playlistPosition < _playlist.Length - 1)
                {
                    _playlistPosition += 1;
                    PlayTrack();
                }

                // If the playlist position is the last on the playlist, move the position to the first position
                else if (_playlist.Length != 0 && _playlistPosition == _playlist.Length - 1)
                {
                    _playlistPosition = 0;
                    PlayTrack();
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "_isBound false");
            }
        }

        /// <summary>
        /// Handle the tracklist button click event
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">EventArgs</param>
        private void _buttonTracklist_Click(object sender, EventArgs e)
        {
            _musicSQLOpenHelper.Close();
            var intentTracklist = new Intent(this, typeof(TracklistActivity));
            StartActivity(intentTracklist);
            Finish();
        }

        /// <summary>
        /// Set repeat status for toggle button click event 
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">EventArgs</param>
        private void _toggleButtonRepeat_Click(object sender, EventArgs e)
        {
            if (!_repeat)
            {
                _repeat = true;
            }
            else
            {
                _repeat = false;
            }
        }

        /// <summary>
        /// Handles the seekbar progress change event by seeking the track to the 
        /// appropriate position.
        /// </summary>
        /// <param name="sender">An object</param>
        /// <param name="e">Eventargs</param>
        private void _seekBarTrack_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "SeekBarTrack_ProgressChanged called, changed to " + e.Progress.ToString());
            // If the event is triggered by user interaction, seek to the position on the playing track
            if (e.FromUser)
            {
                if (_isBound)
                {
                    _musicService.SeekToPosition(e.Progress);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "_isBound false");
                }
            }
        }

        /// <summary>
        /// Updates UI track name, artist name, album name and album art
        /// </summary>
        public void UpdateUI()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "UpdateUI called");

            // Get the track name, album and artist from the database
            string trackName = _musicSQLOpenHelper.GetTrackName(_playlist[_playlistPosition]);
            string trackArtistAlbum = _musicSQLOpenHelper.GetTrackArtist(_playlist[_playlistPosition]) + " - " +
                _musicSQLOpenHelper.GetTrackAlbum(_playlist[_playlistPosition]);

            // Set track name and artist name
            _textViewTrackTitle.SetText(trackName, TextView.BufferType.Normal);
            _textViewTrackArtistAlbum.SetText(trackArtistAlbum, TextView.BufferType.Normal);

            // Make the album art animate-out
            ViewDisappear(_imageViewAlbumArt);

            // Try to get the album art or else show a default image
            try
            {
                // Set the path for the MediaMetadataRetriever
                _mediaMetadataRetriever.SetDataSource(_musicSQLOpenHelper.GetTrackPath(_playlist[_playlistPosition]));

                // Create a bitmap image resource and set it to the imageview
                byte[] bytes = _mediaMetadataRetriever.GetEmbeddedPicture();
                Bitmap bitmap = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                _imageViewAlbumArt.SetImageBitmap(bitmap);
            }
            catch (System.Exception)
            {
                _imageViewAlbumArt.SetImageResource(Resource.Drawable.default_album);
            }

            // Animate-in the album art image view
            ViewAppear(_imageViewAlbumArt);
        }

        /// <summary>
        /// Plays the track at the current id
        /// </summary>
        public void PlayTrack()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "PlayTrack called");
            if (_musicService.IsPlaying())
            {
                _musicService.StopPlayback();
            }
            if ((_playlist.Length != 0 && _playlistPosition < _playlist.Length && _playlistPosition >= 0))
            {
                string trackPath = _musicSQLOpenHelper.GetTrackPath(_playlist[_playlistPosition]);
                _musicService.SetTrackAndPlay(trackPath);
                _seekBarTrack.PostDelayed(UpdateSeekBarPosition, SEEKBAR_UPDATE_DELAY);
                UpdateUI();
            }
        }

        /// <summary>
        /// A runnable to update the seekbar position
        /// </summary>
        private static Runnable UpdateSeekBarPosition = new Runnable(() =>
        {
            if (_musicService.IsPlaying())
            {
                // Surround in a try-catch becasue the method might keep running after the media player is stopped
                try
                {
                    _seekBarTrack.Max = _musicService.GetDuration();
                    //System.Diagnostics.Debug.WriteLine(Constants.ALERT + "PlayActivity - " + "Seekbar max set to "
                    //    + _musicService.GetDuration().ToString());
                    _seekBarTrack.Progress = _musicService.GetCurrentPosition();
                    _seekBarTrack.PostDelayed(UpdateSeekBarPosition, SEEKBAR_UPDATE_DELAY);
                }
                catch (Java.Lang.Exception) { }
            }
        });

        /// <summary>
        /// Handles the Music Service behaviour when a track has finished playback
        /// </summary>
        public void PlaybackComplete()
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "currentTrackComplete called");
            if (!_repeat)
            {
                // If repeat is off, play until the end of the playlist
                if (_playlist.Length != 0 && _playlistPosition + 1 < _playlist.Length && _playlistPosition >= 0)
                {
                    _playlistPosition += 1;
                    PlayTrack();
                }
            }
            else
            {
                // If repeat is on, loop the playlist
                if (_playlist.Length != 0 && _playlistPosition < _playlist.Length - 1)
                {
                    _playlistPosition += 1;
                    PlayTrack();
                }

                // If the playlist position is the last on the playlist, move the position to the first position
                else if (_playlist.Length != 0 && _playlistPosition == _playlist.Length - 1)
                {
                    _playlistPosition = 0;
                    PlayTrack();
                }
            }
        }

        /// <summary>
        /// Makes a given view dissappear after animation
        /// </summary>
        /// <param name="view">A view to me made invisible</param>
        private void ViewAppear(View view)
        {
            Animation appearAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.view_appear);
            view.Visibility = ViewStates.Visible;
            view.StartAnimation(appearAnimation);
        }

        /// <summary>
        /// Makes a given view appear after animation
        /// </summary>
        /// <param name="view">A view to be made visible</param>
        private void ViewDisappear(View view)
        {
            Animation disappearAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.view_disappear);
            view.StartAnimation(disappearAnimation);
            view.Visibility = ViewStates.Invisible;
        }
    }
}