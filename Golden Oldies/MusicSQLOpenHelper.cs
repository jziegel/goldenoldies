using System;
using System.Collections.Generic;

using Android.Content;
using Android.Database.Sqlite;
using Android.Database;

namespace Golden_Oldies
{
    class MusicSQLOpenHelper : SQLiteOpenHelper
    {
        public static string TABLE_NAME = "Tracks";
        public static string COLUMN_ID = "_id";
        public static string COLUMN_TITLE = "Title";
        public static string COLUMN_ARTIST = "Artist";
        public static string COLUMN_ALBUM = "Album";
        public static string COLUMN_PATH = "Path";
        public static string DATABASE_NAME = "Music.db";
        public static int DATABASE_VERSION = 1;

        private static string DATABASE_CREATE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TITLE + " TEXT NOT NULL, "
            + COLUMN_ARTIST + " TEXT NOT NULL, "
            + COLUMN_ALBUM + " TEXT NOT NULL, "
            + COLUMN_PATH + " TEXT);";

        private static string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private static string DELETE_TABLE_ROWS = "DELETE FROM " + TABLE_NAME;

        public MusicSQLOpenHelper(Context context)
            : this(context, DATABASE_NAME, null, DATABASE_VERSION)
        {
        }

        public MusicSQLOpenHelper(Context context, String name, SQLiteDatabase.ICursorFactory factory, Int32 version)
            : base(context, name, factory, version)
        {
        }

        public override void OnCreate(SQLiteDatabase db)
        {
            db.ExecSQL(DATABASE_CREATE);
        }

        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.ExecSQL(DROP_TABLE);
            OnCreate(db);
        }

        /// <summary>
        /// Inserts a data rown into the database
        /// </summary>
        /// <param name="title">Tract title</param>
        /// <param name="artist">Track artist</param>
        /// <param name="album">Track album</param>
        /// <param name="path">Path to the track file</param>
        public void InsertData(string title, string artist, string album, string path)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.Put(COLUMN_TITLE, title);
            contentValues.Put(COLUMN_ARTIST, artist);
            contentValues.Put(COLUMN_ALBUM, album);
            contentValues.Put(COLUMN_PATH, path);

            SQLiteDatabase sqLiteDatabase = this.WritableDatabase;
            sqLiteDatabase.Insert(TABLE_NAME, null, contentValues);
            sqLiteDatabase.Close();
        }

        /// <summary>
        /// Clears the data in the database
        /// </summary>
        public void ClearData()
        {
            SQLiteDatabase sqLiteDatabase = this.WritableDatabase;
            sqLiteDatabase.ExecSQL(DELETE_TABLE_ROWS);
            sqLiteDatabase.Close();
        }

        /// <summary>
        /// Gets a list of the ids of the tracks in the database
        /// </summary>
        /// <returns>A string array with the track IDs</returns>
        public int[] GetIDs()
        {
            List<int> IDs = new List<int>();
            int[] IDsArray = null;
            SQLiteDatabase sqLiteDatabase = this.ReadableDatabase;

            // Get the table where _id = id      
            ICursor cursor = sqLiteDatabase.Query(TABLE_NAME, null, null, null, null, null, COLUMN_ID);

            // Add the items to the list
            while (cursor.MoveToNext())
            {
                int id = cursor.GetInt(cursor.GetColumnIndex(COLUMN_ID));
                IDs.Add(id);
            }

            // Read the list if it is not empty
            if (IDs.Count != 0)
            {
                // Create a string array and add the items to it
                IDsArray = new int[IDs.Count];
                for (int count = 0; count < IDs.Count; count++)
                {
                    IDsArray[count] = IDs[count];
                }
            }

            // Close the database connection
            sqLiteDatabase.Close();

            // Return the array
            return IDsArray;
        }

        /// <summary>
        /// Returns a the path of a track at the given ID as a string
        /// </summary>
        /// <param name="id">The id of the track</param>
        /// <returns>A string containing the path to the track</returns>
        public string GetTrackPath(int id)
        {
            string path = null;
            string idstring = id.ToString();
            string where = COLUMN_ID + " = " + idstring;
            SQLiteDatabase sqLiteDatabase = this.ReadableDatabase;

            // Get the table where _id = id
            ICursor cursor = sqLiteDatabase.Query(TABLE_NAME, null, where, null, null, null, null);

            while (cursor.MoveToNext())
            {
                path = cursor.GetString(cursor.GetColumnIndex(COLUMN_PATH));
            }

            // Close the database connection
            sqLiteDatabase.Close();

            return path;
        }

        /// <summary>
        /// Returns the name of a track at the given ID
        /// </summary>
        /// <param name="id">The id of the track</param>
        /// <returns>A string with the name of the path</returns>
        public string GetTrackName(int id)
        {
            string name = null;
            string idstring = id.ToString();
            string where = COLUMN_ID + " = " + idstring;
            SQLiteDatabase sqLiteDatabase = this.ReadableDatabase;

            // Get the table where _id = ID
            ICursor cursor = sqLiteDatabase.Query(TABLE_NAME, null, where, null, null, null, null);

            while (cursor.MoveToNext())
            {
                name = cursor.GetString(cursor.GetColumnIndex(COLUMN_TITLE));
            }

            // Close the database connection
            sqLiteDatabase.Close();

            return name;
        }

        /// <summary>
        /// Returns the track artist for given track
        /// </summary>
        /// <param name="id">The id of the track</param>
        /// <returns>A string with the artist of the track</returns>
        public string GetTrackArtist(int id)
        {
            string artist = null;
            string idstring = id.ToString();
            string where = "_id = " + idstring;
            SQLiteDatabase sqLiteDatabase = this.ReadableDatabase;

            // Get the table where _id = id
            ICursor cursor = sqLiteDatabase.Query(TABLE_NAME, null, where, null, null, null, null);

            while (cursor.MoveToNext())
            {
                artist = cursor.GetString(cursor.GetColumnIndex(COLUMN_ARTIST));
            }

            // Close the database connection
            sqLiteDatabase.Close();

            return artist;
        }

        /// <summary>
        /// Returns the track album for given track
        /// </summary>
        /// <param name="id">The id of the track</param>
        /// <returns>A string with the album of the track</returns>
        public string GetTrackAlbum(int id)
        {
            string album = null;
            string idstring = id.ToString();
            string where = "_id = " + idstring;
            SQLiteDatabase sqLiteDatabase = this.ReadableDatabase;

            // Get the table where _id = id
            ICursor cursor = sqLiteDatabase.Query(TABLE_NAME, null, where, null, null, null, null);

            while (cursor.MoveToNext())
            {
                album = cursor.GetString(cursor.GetColumnIndex(COLUMN_ALBUM));
            }

            // Close the database connection
            sqLiteDatabase.Close();

            return album;
        }
    }
}