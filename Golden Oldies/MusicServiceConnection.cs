using Android.Content;
using Android.OS;

using Golden_Oldies.Resources.layout;

namespace Golden_Oldies
{
    class MusicServiceConnection : Java.Lang.Object, IServiceConnection
    {
        private string TAG = "MusicServiceConnection - ";
        private PlayActivity _playActivity;
        private MusicServiceBinder _musicServiceBinder;

        public MusicServiceBinder musicServiceBinder
        {
            get
            {
                return _musicServiceBinder;
            }
        }

        public MusicServiceConnection(PlayActivity playActivity)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "constructor called");
            this._playActivity = playActivity;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnServiceConnected called");
            var musicServiceBinder = service as MusicServiceBinder;
            if (musicServiceBinder != null)
            {
                var binder = (MusicServiceBinder)service;
                _playActivity.MusicServiceBinder = binder;
                _playActivity.IsBound = true;

                // Keep instance for preservation across configuration changes
                this._musicServiceBinder = (MusicServiceBinder)service;
                _playActivity.StartPlayback();
                _musicServiceBinder.GetMusicService().setCallbacks(_playActivity);
            }

        }
        public void OnServiceDisconnected(ComponentName name)
        {
            System.Diagnostics.Debug.WriteLine(Constants.ALERT + TAG + "OnServiceDisconnected called");
            _playActivity.IsBound = false;
        }
    }
}