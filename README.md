# README #

Golden Oldies is an Android app developed using Xamarin.Android on Visual Studio Community 2015. The app is a simple music player that is suited to seniors, with a clean and intuitive user interface that focuses on music playback without over-complicated functionality.\

### Features ###

The app allows users to play any track placed in the device's external storage Music folder by scanning and adding music to an internal database on startup. Users can then control music playback by selecting a track and using playback controls. The app uses services to play music so that music can be enjoyed even when the app is not in the foreground.

### What is this repository for? ###

* This repository tracks the latest changes in the Golden Oldies app development.
* Current version: 1.0

### How do I get set up? ###

* Set up requires a working version of Visual Studio and Xamarin for Visual Studio
* All Xamarin dependencies such as the Android SDK Platform and Tools need to be installed (Default with Xamarin installation)
* The Google Android Studio Emulator (with Intel HAXM) provides a better emulation experience, and this is highly recommended.
* Current minimum API level is 19 and as such the relevant APIs need to be downloaded and installed.

### Using and testing the app ###

* Add music to the device's `sdcard/Music` folder. This is the default folder path that the app scans for music.
* The default folder path as well as other app settings can be changed from the `Constants.cs` file.
* Music added should be of `.mp3` file type.
* The app reads the music tracks' metadata to find track information. Music files with complete metadata will provide a better user experience.

### Intro video ###

Watch the into video on [YouTube](https://youtu.be/PnQR6UlIfJs)

### Who do I talk to? ###

* Email Jerome: jziegel@deakin.edu.au