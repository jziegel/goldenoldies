====================================================================================
   Graphics
====================================================================================

O	Retro vinyl record player designed by Freepik.com
	http://www.freepik.com/free-vector/retro-vinyl-record-player_720863.htm
	Under Freepik.com License

O 	Some Assets were generated using Android Asset Studio
	https://romannurik.github.io/AndroidAssetStudio/index.html
	Under Creative Commons Attribution 3.0 Unported License
	https://creativecommons.org/licenses/by/3.0/

All other graphics and assets designed by the developer.


====================================================================================
    Audio
====================================================================================

O	Water Lily - The 126ers
	Under CC0 1.0 Universal License
	https://creativecommons.org/publicdomain/zero/1.0/deed.en

	
====================================================================================
    Fonts
====================================================================================

O	Comfortaa Regular by Johan Aakerlund
	https://plus.google.com/+JohanAakerlund/about
	Used under SIL Open Font License, 1.1
	http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL